#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# Created by Farhiya Jama Nur, Garth Jackson, Amalia Kerkine Keramida and Nicole Ghessen
from __future__ import (absolute_import, division, print_function)
from unicodedata import name
__metaclass__ = type

DOCUMENTATION = r'''
---
module: ELK_space

short_description: This module creates a space in ELK

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: Interacts with Kibana site to create a space.
 
options:
    id:
        description: This is the unique id you give to the space (conventionally, same as name) and cannot be changed
        required: true
        type: str

    name:
        description: This is the unique name you give to the space (conventionally, same as id)
        required: true
        type: str

    api_url:
        description: 
            -   Written as: localhost:5601/api/spaces/space
            -   Api connection for creating space on kibana 
        required: true
        type: str
    
    description:
        description: The description for the space
        required: false
        type: str
    
    disabledFeatures:
        description: The list of disabled features for the space
        required: false
        type: list
    
    initials:
        description: 
        -   The initials shown in the space avatar
        -   By default, the initials are automatically generated from the space name 
        -   Initials must be 1 or 2 characters
        required: false
        type: str
    
    color:
        description:
        -   The hexadecimal color code used in the space avatar
        -   By default, the color is automatically generated from the space name
        required: false
        type: str
    
    imageUrl:    
        description:
        -   The data-URL encoded image to display in the space avatar
        -   If specified, initials will not be displayed, and the color will be visible as the background color for transparent images
        -   For best results, your image should be 64x64
        -   Images will not be optimized by this API call, so care should be taken when using custom images
        required: false
        type: str

    




# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Create simple space
- name: create space in kibana
      ELK_space:
        id: my_space
        name: my_space
        api_url: http://0.0.0.0:5601/api/spaces/space

# pass in a message and have changed true
- name: create space in kibana
      ELK_space:
        id: my_space
        name: my_space
        api_url: http://0.0.0.0:5601/api/spaces/space
        description: my first space
        disabledFeatures: []
        initals: ms
        color: \#FF5733 (color code starts with hash symbol, the \ stops it from being a comment )


# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule
from wsgiref import headers
import requests
import json


def get_role(api_url):
    role = requests.get(api_url)
    
    return role

def check_for_changes(role):
    if str(role) == '<Response [200]>':
        state = False
    else:
        state = True

    return state

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name=dict(type='str', required=True),
        metadata=dict(type='dict', required=False),
        kibana=dict(type='list', required=False),
        elasticsearch=dict(type='dict', required=False)
    )
    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=' ',
        original_message='',
        message='checking for changes'
    )


    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    api_url = 'http://0.0.0.0:9200/security/role/' + module.params['name']
    role = get_role(api_url=api_url)

    state = check_for_changes(role)
    result['changed'] = state

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode


    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    # name = module.params['name']
    # result['message'] = f'New space added:{module.params['name']}'
    if result['changed'] == True:
        data = {
            'metadata': module.params['metadata'],
            'kibana': module.params['kibana'],
            'elasticsearch': module.params['elasticsearch']
            }
            
        headers = {
            'kbn-xsrf':'true',
            'Content-Type': 'application/json'
            }

        response = requests.put(data=json.dumps(data), url=api_url, headers=headers)
        response_decoded = response.json()

        result['original_message'] = f'New space has been created: { response_decoded}'
    
#    else:
#        result['original_message'] = 'Space already exists. No new space has been created.'
    
    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if module.params['name'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()

