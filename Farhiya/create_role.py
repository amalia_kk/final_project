from wsgiref import headers
import requests
import json

url = 'http://3.249.215.135:9200/security/role/my_admin_role'
# headers = {'Content-Type': 'application/json'}
headers = {
        'kbn-xsrf':'true',
        'Content-Type': 'application/json'
        }

data={
  "metadata" : {
    "version" : 1
  },
  "kibana": [
    {
      "base": ["all"],
      "feature": {
      },
      "spaces": [
        "default"
      ]
    }
  ]
}
response = requests.put(url=url, data=json.dumps(data), headers=headers)

print(json.dumps(data))
print(response.json())

# below needs to be added to playbook and kibana needs to be restarted
# xpack.security.enabled: true ---> sudo nano /etc/elasticsearch/elasticsearch.yml inside the node and xpack.security.transport.ssl.enabled: true

