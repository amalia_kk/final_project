from wsgiref import headers
import requests
import json


spaces = {'id':'test', 'name':'tester'}
headers = {'kbn-xsrf':'true'}
base_url = 'http://3.249.215.135:5601/api/spaces/space'

def get():

    response = requests.get(base_url)
    r = response.json()

    print(response)
    print(r)
    return r

def check_id():

    result=False
    # my_data = {'id':'default', 'name': 'Default'}
    # my_data= {'id': 'derek', 'name': 'zoolander', 'disabledFeatures': []}

    my_data = {'id':'DEault', 'name': 'DFaUlT'}

    spaces = [{'id': 'default', 'name': 'Default', 'description': 'This is your default space!', 'color': '#00bfb3', 'disabledFeatures': [], '_reserved': True}, {'id': 'hiccup', 'name': 'how to train your dragons', 'disabledFeatures': []}, {'id': 'sales', 'name': 'sales', 'disabledFeatures': []}, {'id': 'test', 'name': 'tester', 'disabledFeatures': []}, {'id': 'timmy', 'name': 'turner', 'disabledFeatures': []}, {'id': 'derek', 'name': 'zoolander', 'disabledFeatures': []}]
    

    for space in spaces:

        if my_data['id'].lower() == space['id'].lower():
            if my_data['name'].lower() == space['name'].lower():
                result=False
                break
        else:
            result=True

    return result

def post():

    response = requests.post(spaces=spaces, url=base_url, headers=headers)
    respone_decoded = response.json()

    print(response)
    print(respone_decoded)


# get()    
# post()
# check_id()

def get_spaces(api_url):
    spaces = requests.get(api_url).json()
    print(spaces)
    return spaces

get_spaces(base_url) 