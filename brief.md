# Project Ansible Modules Pro

As devops company we work a lot with Ansible and have several ansible modules. 

We need a framework to develop Ansible modules and we should be so proud of them we could even publish it to the community!

### Task

Your task is to:

First Part 
- Learn how to build ansible modules
- Discuss and outline best practices
- Build an ansible module for ELK
- How do you know your module works? Are there tests?


Second Part:
- Once have build and deployed your code go find more people to build more Ansible Modules!
- Build module to setup things
- Build modules to test working state of API's
- Build clean maintainable code that others can the work on

Ask your product owner and any other mean necessary to extra at least two more ansible modules you can build. 

### Background 

ELK stands for Elastic Logstash Kibana, this is a monitoring system we use. 

We manage several of these with Asnible. 

We're going to write some modules to 


### Current user stories

As a AL Resource Manager, I need to know my team is able to excel at python and Ansible, so that I can confortably set them on a task and have it done. 

As a AL Resource Manager, I want my team to creat, publish and maintain opensource Ansible modules, so that we contribute to the community and standout as experts.

**ELK specific modules**

As an engineer I would like to create an Anslible module for ELK, so that I can do operation on ELK using ansible and on different hosts.


As an engineer I would like to ´create spaces´ in ELK setups, so that I don't have to manually loggin and risk human error.


As an engineer I would like to ´create indixes´ in ELK setups, so that I don't have to manually loggin and risk human error.

