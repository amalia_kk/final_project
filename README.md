# Creating modules for ELK stack

In this repo you will find custom modules that automate the creation of some ELK processes.

## What is ELK

ELK is an acronym that stands for 3 open source tool that works in unison. 

Elasticsearch, Logstash and Kibana. 
Elasticsearch is a search and analytic engine.
Logstash is a server-side data processing pipeline. It takes in data, transforms it and sends it to Elasticsearch.
Kibana lets users visualise their data.


## Our Custom modules

In the Modules folder you will find modules we created for an ELK stack, which are to used to create a space, create an index and create a role. If you wish to use these modules copy them into your ansible controller machine and add the module to `~/ansible/lib/ansible/modules`. 

## Process of module creation

1. Manually
   - Try to do the task manually, so you can see what steps go into it.
  
2. Terminal 
   -  When trying to send and recieve data from a site you can do this through the terminal using a curl command.

3. Python
   - Write a python script that uses requests library to send and recieve data.

4. Module
    -   Go to https://docs.ansible.com/ansible/latest/dev_guide/developing_modules_general.html
    -  Use the template for writing the module
    -  In the first section of the run module function, define your variables
    -  In the fifth section of the run module function, this is where the useful code. In our case this is where a space is created on our ELK stack through an api.

5. Playbook
   - In the playbook add your creat space function.
   - It should follow the template: 

   ```bash
   name: 'description of what the module does'
   name_of_module:
      <argument_1>:
      <argument_2>:
      <argument_3>:

   name: 'create space in kibana'
   ELK_space:
     id: lisa
     name: lisa
     color: #32a838

   ```

   
   In the create space module there are some required arguments/parameters that must be stated when calling the module in the playbook, id and name. There are also some optional arguments like color.


## Testing

Once the modules had been created, we decided to carry out some testing. The goal of unit testing is to detect bugs and inconsistencies as soon as possible. By having these tests, it allows future developers to locate errors more quickly than diving into the code. This assists in maintainability and efficiency. As a result, a reliable engineering environment is ensured.

Pytest is a code-level test, written in Python. We decided to use this method of testing since our modules are also written in Python and so there is an element of compatibility. In addition to this, the Ansible documentation for creating modules recommends to use it. Pytest allows us to write simple and scalable tests and is used to verify a small unit of functionality. So in our project, the script to create the modules was made up of multiple functions and a test would be carried out on an individual function. If the function passed the test, it would mean that it was ready to be used in the module. 

Another method of testing that we had a look at is smoke testing; otherwise known as “build verification testing” or “confidence testing”, is the process that determines whether the deployed software build is stable or not. In addition, it provides confirmation to the QA team to proceed with further testing. It consists of a minimal set of tests run on each build to test software functionalities.


## Deployment

