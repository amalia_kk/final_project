terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = "Academy"
  shared_credentials_file = "/Users/nicoleghessen/Downloads/credentials"
  region = var.aws_region
}

/*
Create EC2 Instances- 1 for the ansible controller and 2 for the ELK stack
*/

resource "aws_instance" "FANG-Ansible-Controller" {
  ami               = var.fang_controller_ami
  key_name          = "Cohort9_shared"
  security_groups   = var.security_groups
  user_data         = "${base64encode(data.template_file.FANG_inventory.rendered)}"
  subnet_id         = "subnet-7bddfc1d"
  instance_type     = "t2.micro"
  tags = {
    Name = "${var.my_tag}_ansible_controller-testing-main"
  }
}

resource "aws_instance" "elk-agent1" {
  ami               = var.elk_agent_ami1
  key_name          = "Cohort9_shared"
  security_groups   = var.security_groups
  subnet_id         = "subnet-7bddfc1d"
  instance_type     = "t2.medium"
  tags = {
    Name = "${var.my_tag}_elk_agent-testing1"
  }
}

resource "aws_instance" "elk-agent2" {
  ami               = var.elk_agent_ami2
  key_name          = "Cohort9_shared"
  security_groups   = var.security_groups
  subnet_id         = "subnet-7bddfc1d"
  instance_type     = "t2.medium"
  tags = {
    Name = "${var.my_tag}_elk_agent-testing2"
  }
}


data "template_file" "FANG_inventory" {
  template = "${file("user_data.tpl")}"
  vars = {
    consul_address1 = "${aws_instance.elk-agent1.private_ip}"
    consul_address2 = "${aws_instance.elk-agent2.private_ip}"
  }
}