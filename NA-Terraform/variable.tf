variable "my_tag" {
	description = "Name to tag all elements with"
	default = "FANG"
}

variable "aws_region" {
    description = "AWS region"
    default = "eu-west-1"
}

variable "fang_controller_ami" {
    description = "FANG Ansible Controller AMI"
    default = "ami-0fdeda39a9cf6d1d3"
}


variable "elk_agent_ami1" {
    description = "Instances for ELK stack"
    default = "ami-0e61e798521d249ca"
   
}

variable "elk_agent_ami2" {
    description = "Instances for ELK stack"
    default = "ami-0e61e798521d249ca"
   
}

variable "security_groups" {
    description = "c9 and jenkins"
    default = ["sg-026a52fbbe4783f73", "sg-072a803b313e9637f"]
}