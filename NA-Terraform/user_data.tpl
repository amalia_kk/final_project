#!/bin/bash

sudo sh -c 'cat > /home/ubuntu/ELK-setup/inventory <<_END_

[all]
${consul_address1} ansible_ssh_common_args='-o StrictHostKeyChecking=no'
${consul_address2} ansible_ssh_common_args='-o StrictHostKeyChecking=no'

'

sleep 120 

source /home/ubuntu/ansible/venv/bin/activate && source /home/ubuntu/ansible/hacking/env-setup




ansible-playbook -i /home/ubuntu/ELK-setup/inventory /home/ubuntu/ELK-setup/playbook.yml


