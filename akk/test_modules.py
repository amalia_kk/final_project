from ELKspace import *
from elkindex import *

# Tests for creating a space module:

# Testing if python will return all existing spaces
def test_spaces():
    api_url = 'http://54.216.180.158:5601/api/spaces/space'
    actual = get_spaces(api_url)
    expected = [{'id': 'default', 'name': 'Default', 'description': 'This is your default space!', 'color': '#00bfb3', 'disabledFeatures': [], '_reserved': True}, {'id': 'mag', 'name': 'mag', 'disabledFeatures': []}]
    assert actual == expected 

# Testing if the user is trying to create a new space (if there has been a change)
def test_changes():
    spaces = [{'id': 'default', 'name': 'Default', 'description': 'This is your default space!', 'color': '#00bfb3', 'disabledFeatures': [], '_reserved': True}, {'id': 'mag', 'name': 'mag', 'disabledFeatures': []}]
    name = 'hello'
    id = 'bye'
    actual = check_for_changes(spaces, id, name)
    expected = True
    assert actual == expected 




# Tests on creating an index module

# Testing if python can retrieve existing indices
def test_get_index():
    request_url = 'http://54.216.180.158:9200/_cat/indices/'
    actual = get_index(request_url)
    expected = '<Response [200]>'
    assert actual == expected


# Test to see if a change has been made to indices
def test_changes_index():
    index = get_index('http://54.216.180.158:9200/_cat/indices/')
    expected = False
    actual = check_for_changess(index)
    assert expected == actual

